<?php
use Symfony\Component\Yaml\Yaml;

$config = Yaml::parseFile(__DIR__ .'/config.yml');

return $config;
