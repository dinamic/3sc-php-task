<?php

namespace Tsc\CatStorageSystem\Tests;

use PHPUnit\Framework\TestCase;
use Tsc\CatStorageSystem\Directory as TestClass;

class DirectoryTest extends TestCase
{
    /**
     * @var TestClass
     */
    private $instance;

    public function setUp()
    {
        $this->instance = new TestClass();
    }

    public function testGetNameWouldWork()
    {
        $randomName = random_bytes(5);
        static::assertSame($this->instance, $this->instance->setName($randomName));
        static::assertSame($randomName, $this->instance->getName());
    }

    public function testGetPathWouldWork()
    {
        $randomString = random_bytes(5);
        static::assertSame($this->instance, $this->instance->setPath($randomString));
        static::assertSame($randomString, $this->instance->getPath());
    }

    public function testGetCreatedTimeWouldWork()
    {
        $dateTime = new \DateTimeImmutable();
        static::assertSame($this->instance, $this->instance->setCreatedTime($dateTime));
        static::assertSame($dateTime, $this->instance->getCreatedTime());
    }
}
