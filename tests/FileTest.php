<?php

namespace Tsc\CatStorageSystem\Tests;

use PHPUnit\Framework\TestCase;
use Tsc\CatStorageSystem\Directory;
use Tsc\CatStorageSystem\File as TestClass;

class FileTest extends TestCase
{
    /**
     * @var TestClass
     */
    private $instance;

    public function setUp()
    {
        $this->instance = new TestClass;
    }

    public function testGetNameWouldWork()
    {
        $randomName = random_bytes(5);
        static::assertSame($this->instance, $this->instance->setName($randomName));
        static::assertSame($randomName, $this->instance->getName());
    }

    public function testGetSizeWouldWork()
    {
        $randomName = random_int(999, 99999);
        static::assertSame($this->instance, $this->instance->setSize($randomName));
        static::assertSame($randomName, $this->instance->getSize());
    }

    public function testGetParentDirectoryWouldWork()
    {
        $directory = static::createMock(Directory::class);

        static::assertSame($this->instance, $this->instance->setParentDirectory($directory));
        static::assertSame($directory, $this->instance->getParentDirectory());
    }

    public function testGetCreatedTimeWouldWork()
    {
        $dateTime = new \DateTimeImmutable();
        static::assertSame($this->instance, $this->instance->setCreatedTime($dateTime));
        static::assertSame($dateTime, $this->instance->getCreatedTime());
    }

    public function testGetModifiedTimeWouldWork()
    {
        $dateTime = new \DateTimeImmutable();
        static::assertSame($this->instance, $this->instance->setModifiedTime($dateTime));
        static::assertSame($dateTime, $this->instance->getModifiedTime());
    }

    public function testGetPathWillBeCalculatedUsingParentDirectory()
    {
        $directory = static::createMock(Directory::class);
        $directory
            ->expects(static::once())
            ->method('getPath')
            ->willReturn('/path/to/directory')
        ;
        $directory
            ->expects(static::once())
            ->method('getName')
            ->willReturn('directoryName')
        ;

        $this->instance->setName('testFileNameHere');

        static::assertSame($this->instance, $this->instance->setParentDirectory($directory));
        static::assertSame('/path/to/directory/directoryName/testFileNameHere', $this->instance->getPath());
    }
}
