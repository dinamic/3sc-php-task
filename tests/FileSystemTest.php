<?php

namespace Tsc\CatStorageSystem;

function unlink($filepath)
{
    return \Tsc\CatStorageSystem\Tests\FileSystemTest::$functions->unlink($filepath);
}

namespace Tsc\CatStorageSystem\Tests;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Tsc\CatStorageSystem\File;
use Tsc\CatStorageSystem\FileSystem as TestClass;
use Tsc\CatStorageSystem\SplFileInfoFactory;
use Tsc\CatStorageSystem\SplFileObjectFactory;

class FileSystemTest extends TestCase
{
    /**
     * @var MockObject
     */
    public static $functions;

    /**
     * @var SplFileInfoFactory|MockObject
     */
    protected $splFileInfoFactory;

    /**
     * @var SplFileObjectFactory|MockObject
     */
    protected $splFileObjectFactory;

    /**
     * @var TestClass
     */
    protected $instance;

    public function setUp()
    {
        $this->splFileInfoFactory = static::createMock(SplFileInfoFactory::class);
        $this->splFileObjectFactory = static::createMock(SplFileObjectFactory::class);

        $this->instance = new TestClass($this->splFileInfoFactory, $this->splFileObjectFactory);

        static::$functions = static::getMockBuilder(\stdClass::class)
            ->setMethods([
                'unlink',
            ])
            ->getMock()
        ;

        parent::setUp();
    }

    public function testDeleteFileWillThrowExceptionIfThereIsDirectoryWithTheSameName()
    {
        $file = static::createMock(File::class);
        $file
            ->expects(static::once())
            ->method('getPath')
            ->willReturn('/path/to/file')
        ;

        $splFileObject = $this->getMockBuilder(\SplFileObject::class)
            ->setConstructorArgs([__FILE__])
            ->getMock()
        ;

        $this->splFileObjectFactory
            ->expects(static::once())
            ->method('create')
            ->with('/path/to/file')
            ->willReturn($splFileObject)
        ;

        $splFileObject
            ->expects(static::once())
            ->method('isDir')
            ->willReturn(true)
        ;

        static::expectException(\LogicException::class);
        static::expectExceptionMessage('File is directory.');

        $this->instance->deleteFile($file);
    }

    public function testDeleteFileWillDeleteFile()
    {
        $file = static::createMock(File::class);
        $file
            ->expects(static::exactly(2))
            ->method('getPath')
            ->willReturn('/path/to/file')
        ;

        $splFileObject = $this->getMockBuilder(\SplFileObject::class)
            ->setConstructorArgs([__FILE__])
            ->getMock()
        ;

        $this->splFileObjectFactory
            ->expects(static::once())
            ->method('create')
            ->with('/path/to/file')
            ->willReturn($splFileObject)
        ;

        $splFileObject
            ->expects(static::once())
            ->method('isDir')
            ->willReturn(false)
        ;

        static::$functions
            ->expects(static::once())
            ->method('unlink')
            ->with('/path/to/file')
            ->willReturn(true)
        ;

        static::assertTrue($this->instance->deleteFile($file));
    }
}
