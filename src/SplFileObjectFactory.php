<?php

namespace Tsc\CatStorageSystem;

class SplFileObjectFactory
{
    public function create($filePath)
    {
        return new \SplFileObject($filePath);
    }
}
