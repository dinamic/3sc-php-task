<?php
namespace Tsc\CatStorageSystem;

use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;
use DirectoryIterator;

class FileSystem implements FileSystemInterface
{
    /**
     * @var SplFileInfoFactory
     */
    private $splFileInfoFactory;

    /**
     * @var SplFileObjectFactory
     */
    private $splFileObjectFactory;

    public function __construct(SplFileInfoFactory $splFileInfoFactory, SplFileObjectFactory $splFileObjectFactory)
    {
        $this->splFileInfoFactory = $splFileInfoFactory;
        $this->splFileObjectFactory = $splFileObjectFactory;
    }

    public function createFile(FileInterface $file, DirectoryInterface $parent)
    {
        touch($parent->getPath() .'/'. $parent->getName() .'/'. $file->getName(), $file->getModifiedTime()->format('U'));

        return $this;
    }

    /**
     * @param FileInterface $file
     *
     * @return FileInterface
     */
    public function updateFile(FileInterface $file)
    {
        $splFile = $this->splFileObjectFactory->create($file->getPath() .'/'. $file->getName());

        touch($splFile->getPathname(), $file->getModifiedTime()->format('U'));

        return $file;
    }

    /**
     * @param FileInterface $file
     * @param string $newName
     *
     * @return FileInterface
     */
    public function renameFile(FileInterface $file, $newName)
    {
        $oldFile = $this->splFileObjectFactory->create($file->getPath());
        $newFile = $this->splFileInfoFactory->create($oldFile->getPath() .'/'. $newName);

        if ($newFile->isDir() || $newFile->isFile() || $newFile->isReadable()) {
            throw new \RuntimeException('Destination already exists: '. $newFile->getPathname());
        }

        rename($oldFile->getPathname(), $newFile->getPathname());

        $file->setName($newName);

        return $file;
    }

    /**
     * @param FileInterface $file
     *
     * @return bool
     */
    public function deleteFile(FileInterface $file)
    {
        $splFile = $this->splFileObjectFactory->create($file->getPath());

        if ($splFile->isDir()) {
            throw new \LogicException('File is directory.');
        }

        return unlink($file->getPath());
    }

    /**
     * @param DirectoryInterface $directory
     *
     * @return DirectoryInterface
     */
    public function createRootDirectory(DirectoryInterface $directory)
    {
        $directoryPathname = $directory->getPath() .'/'. $directory->getName();

        if (!mkdir($directoryPathname, '0775') && !is_dir($directoryPathname)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $directory->getPath()));
        }

        return $directory;
    }

    /**
     * @param DirectoryInterface $directory
     * @param DirectoryInterface $parent
     *
     * @return DirectoryInterface
     */
    public function createDirectory(
        DirectoryInterface $directory,
        DirectoryInterface $parent
    ) {
        $parentDirectoryPathname = $parent->getPath() .'/'. $parent->getName();
        $directoryPathname = $parentDirectoryPathname .'/'. $directory->getName();

        if (!mkdir($directoryPathname, '0775') && !is_dir($directoryPathname)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $parent->getPath()));
        }

        return $directory;
    }

    /**
     * @param DirectoryInterface $directory
     *
     * @return bool
     */
    public function deleteDirectory(DirectoryInterface $directory)
    {
        if ($this->getDirectoryCount($directory) > 0) {
            throw new \LogicException('Cannot remove directory - subdirectories exist.');
        }

        if ($this->getFileCount($directory) > 0) {
            throw new \LogicException('Cannot remove directory - files exist.');
        }

        return unlink($directory->getPath() .'/'. $directory->getName());
    }

    /**
     * @param DirectoryInterface $directory
     * @param string $newName
     *
     * @return DirectoryInterface
     */
    public function renameDirectory(DirectoryInterface $directory, $newName)
    {
        $oldFile = $this->splFileInfoFactory->create($directory->getPath() .'/'. $directory->getName());
        $newFile = $this->splFileInfoFactory->create($directory->getPath() .'/'. $newName);

        if ($newFile->isDir() || $newFile->isFile() || $newFile->isReadable()) {
            throw new \RuntimeException(sprintf(
                'Unable to rename directory. File already exists at "%s".',
                $newFile->getPathname()
            ));
        }

        rename($oldFile->getPathname(), $newFile->getPathname());

        return $directory->setName($newName);
    }

    /**
     * @param DirectoryInterface $directory
     *
     * @return int
     */
    public function getDirectoryCount(DirectoryInterface $directory)
    {
        $i = 0;

        foreach (new DirectoryIterator($directory->getPath() .'/'. $directory->getName()) as $file) {
            if ($file->isDir() && !$file->isDot()) {
                ++$i;
            }
        }

        return $i;
    }

    /**
     * @param DirectoryInterface $directory
     *
     * @return int
     */
    public function getFileCount(DirectoryInterface $directory)
    {
        $i = 0;

        foreach (new DirectoryIterator($directory->getPath() .'/'. $directory->getName()) as $file) {
            if ($file->isDot() || !$file->isFile()) {
                continue;
            }

            ++$i;
        }

        return $i;
    }

    /**
     * @param DirectoryInterface $directory
     *
     * @return int
     */
    public function getDirectorySize(DirectoryInterface $directory)
    {
        $bytesTotal = 0;

        $path = realpath($directory->getPath() .'/'. $directory->getName());

        if ($path !== false && $path !== '' && file_exists($path)) {
            foreach (new \RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS)) as $object) {
                $bytesTotal += $object->getSize();
            }
        }

        return $bytesTotal;
    }

    /**
     * @param DirectoryInterface $destination
     *
     * @return DirectoryInterface[]
     */
    public function getDirectories(DirectoryInterface $destination)
    {
        $directories = [];
        $destinationPathname = $destination->getPath();

        if (strlen($destination->getName()) > 0) {
            $destinationPathname .= '/'. $destination->getName();
        }

        foreach (new DirectoryIterator($destinationPathname) as $file) {
            if (!$file->isDir() || $file->isDot()) {
                continue;
            }

            $directory = new Directory();
            $directory
                ->setName($file->getFilename())
                ->setPath($file->getPath())
                ->setCreatedTime(\DateTimeImmutable::createFromFormat('U', $file->getCTime()))
            ;

            $directories[] = $directory;
        }

        return $directories;
    }

    /**
     * @param DirectoryInterface $directory
     *
     * @return FileInterface[]
     */
    public function getFiles(DirectoryInterface $directory)
    {
        $files = [];

        $directoryPathname = $directory->getPath();
        if (strlen($directory->getName()) > 0) {
            $directoryPathname .= '/'. $directory->getName();
        }

        foreach (new DirectoryIterator($directoryPathname) as $fileInfo) {
            if (!$fileInfo->isFile()) {
                continue;
            }

            /**
             * Linux does not hold the creation time as such.
             * @see https://askubuntu.com/questions/62492/how-can-i-change-the-date-modified-created-of-a-file
             * @see https://stackoverflow.com/questions/4401320/php-how-can-i-get-file-creation-date
             */
            $file = new File();
            $file
                ->setName($fileInfo->getFilename())
                ->setSize($fileInfo->getSize())
                ->setParentDirectory($directory)
                ->setCreatedTime(\DateTimeImmutable::createFromFormat('U', $fileInfo->getCTime()))
                ->setModifiedTime(\DateTimeImmutable::createFromFormat('U', $fileInfo->getMTime()))
            ;

            $files[] = $file;
        }

        return $files;
    }
}
