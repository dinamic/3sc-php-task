<?php 
namespace Tsc\CatStorageSystem\Command;

use Symfony\Component\Console\Helper\Table;
use Tsc\CatStorageSystem;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DirectoryListCommand extends AbstractCommand
{
    public function configure()
    {
        $this
            ->setName('tsc:list:directories')
            ->setDescription('get directory list')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $fileSystem = $this->getFileSystem();

        $directory = new CatStorageSystem\Directory();
        $directory
            ->setPath($this->config['filesystem']['root'])
        ;

        $directories = $fileSystem->getDirectories($directory);

        $table = new Table($output);

        $table
            ->setHeaders(array(
                'Directory',
                'Size',
                'Subdirectories',
                'Files',
                'Last Updated At',
            ))
        ;

        foreach ($directories as $directory) {
            $table->addRow([
                $directory->getName(),
                $fileSystem->getDirectorySize($directory),
                $fileSystem->getDirectoryCount($directory),
                $fileSystem->getFileCount($directory),
                $directory->getCreatedTime()->format(\DateTimeInterface::RFC822)
            ]);
        }

        $table->render();
    }
}
