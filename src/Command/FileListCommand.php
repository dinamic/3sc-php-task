<?php 
namespace Tsc\CatStorageSystem\Command;

use Symfony\Component\Console\Helper\Table;
use Tsc\CatStorageSystem;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FileListCommand extends AbstractCommand
{
    public function configure()
    {
        $this
            ->setName('tsc:list:files')
            ->setDescription('get directory list')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $directory = new CatStorageSystem\Directory();
        $directory->setPath($this->config['filesystem']['root']);

        $files = $this->getFileSystem()->getFiles($directory);

        $table = new Table($output);

        $table
            ->setHeaders(array(
                'Filename',
                'Size',
                'Created At',
                'Last Modified At',
            ))
        ;

        foreach ($files as $file) {
            $table->addRow([
                $file->getName(),
                $file->getSize(),
                $file->getCreatedTime()->format(\DateTimeInterface::RFC822),
                $file->getModifiedTime()->format(\DateTimeInterface::RFC822),
            ]);
        }

        $table->render();
    }
}
