<?php 
namespace Tsc\CatStorageSystem\Command;

use Tsc\CatStorageSystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FileRenameCommand extends AbstractCommand
{
    public function __construct()
    {
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('tsc:file:rename')
            ->setDescription('Rename name')
            ->setHelp('This command allows you to interact with the file once name is set')
            ->addArgument('oldfilename', InputArgument::REQUIRED, 'The filename of the cat-gif.')
            ->addArgument('newfilename', InputArgument::REQUIRED, 'The new filename of the cat-gif.')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $directory = new CatStorageSystem\Directory();
        $directory
            ->setPath($this->getFileSystemRoot())
        ;

        $file = new CatStorageSystem\File();
        $file
            ->setName($input->getArgument('oldfilename'))
            ->setParentDirectory($directory)
        ;

        $this->getFileSystem()->renameFile($file, $input->getArgument('newfilename'));

        if ($input->getArgument('oldfilename') !== $file->getName()) {
            //rename must have been successful.
            $output->writeln('File ' . $input->getArgument('oldfilename') . ' renamed to ' . $input->getArgument('newfilename'));
        } else {
            $output->writeln('<error>File rename unsuccessful</error>');
        }
    }
}
