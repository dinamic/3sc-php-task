<?php 
namespace Tsc\CatStorageSystem\Command;

use Tsc\CatStorageSystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateRootDirectoryCommand extends AbstractCommand
{
    public function configure()
    {
        $this
            ->setName('directory:createRootDirectory')
            ->setDescription('create root directory')
            ->setHelp('create root directory')
            ->addArgument('name', InputArgument::OPTIONAL, 'The name of the directory')
        ;
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $directory = new CatStorageSystem\Directory();
        $directory->setName($input->getArgument('name'));

        $fileSystem = $this->getFileSystem();
        $fileSystem->createRootDirectory($directory);

        foreach ($fileSystem->getDirectories($directory) as $directory) {
            $output->writeln($directory->getName());
        }
    }
}
