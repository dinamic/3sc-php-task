<?php

namespace Tsc\CatStorageSystem\Command;

use Symfony\Component\Console\Command\Command;
use Tsc\CatStorageSystem;

abstract class AbstractCommand extends Command
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @param array $config
     * @return AbstractCommand
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return CatStorageSystem\FileSystem
     */
    protected function getFileSystem()
    {
        return new CatStorageSystem\FileSystem(
            new CatStorageSystem\SplFileInfoFactory(),
            new CatStorageSystem\SplFileObjectFactory()
        );
    }

    /**
     * @return string
     */
    protected function getFileSystemRoot()
    {
        return $this->config['filesystem']['root'];
    }
}
