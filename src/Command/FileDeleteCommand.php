<?php 
namespace Tsc\CatStorageSystem\Command;

use Tsc\CatStorageSystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FileDeleteCommand extends AbstractCommand
{
    public function configure()
    {
        $this
            ->setName('tsc:file:delete')
            ->setDescription('delete a file')
            ->addArgument('filename', InputArgument::REQUIRED, 'The filename of the cat-gif.')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $fileSystem = $this->getFileSystem();

        $directory = new CatStorageSystem\Directory();
        $directory
            ->setPath($this->getFileSystemRoot())
        ;

        $file = new CatStorageSystem\File();
        $file
            ->setName($input->getArgument('filename'))
            ->setParentDirectory($directory)
        ;

        if ($fileSystem->deleteFile($file)) {
            $output->writeln($file->getName() . ' deleted');
        } else {
            $output->writeln(sprintf('<error>%s not deleted</error>', $file->getName()));
        }
    }
}
