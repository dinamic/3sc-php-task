<?php
namespace Tsc\CatStorageSystem;

use \DateTimeInterface;
use \SplFileInfo;
use DateTime;

class File implements FileInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $size;

    /**
     * @var DateTimeInterface
     */
    private $createdAt;

    /**
     * @var DateTimeInterface
     */
    private $modifiedAt;

    /**
     * @var DirectoryInterface
     */
    private $parentDirectory;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     *
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Linux does not hold the creation time as such. This returns the inode change time;
     * @see https://askubuntu.com/questions/62492/how-can-i-change-the-date-modified-created-of-a-file
     * @see https://stackoverflow.com/questions/4401320/php-how-can-i-get-file-creation-date
     *
     * @return DateTimeInterface
     */
    public function getCreatedTime()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface $createdAt
     *
     * @return $this
     */
    public function setCreatedTime(DateTimeInterface $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getModifiedTime()
    {
        return $this->modifiedAt;
    }

    /**
     * @param DateTimeInterface $modifiedAt
     *
     * @return $this
     */
    public function setModifiedTime(DateTimeInterface $modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
        return $this;
    }

    /**
     * @return DirectoryInterface
     */
    public function getParentDirectory()
    {
        return $this->parentDirectory;
    }

    /**
     * @param DirectoryInterface $directory
     *
     * @return $this
     */
    public function setParentDirectory(DirectoryInterface $directory)
    {
        $this->parentDirectory = $directory;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->parentDirectory->getPath() .'/'. $this->parentDirectory->getName() .'/'. $this->name;
    }
}
