<?php

namespace Tsc\CatStorageSystem;

class SplFileInfoFactory
{
    public function create($filePath)
    {
        return new \SplFileInfo($filePath);
    }
}
